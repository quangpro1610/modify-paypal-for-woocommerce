<?php
/**
 * Single Product title
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<h1 class="product-title product_title entry-title">
	<?php the_title(); ?>
</h1>

<?php if(get_theme_mod('product_title_divider', 1)) { ?>
	<div class="is-divider small"></div>
<?php } ?>
<?php echo '<b>Author:</b> '; ?>
<?php echo the_field('bookauthor');
      echo "<br>"; ?>
<?php echo '<b>Edition:</b> '; ?>
<?php echo the_field('book_edition'); 
      echo "<br>"; ?>
<?php echo '<b>ISBN13:</b> '; ?>
<?php echo the_field('isbn13');
      echo "<br>"; ?>
<?php echo '<b>Format:</b> '; ?>
<?php echo the_field('format'); ?>
