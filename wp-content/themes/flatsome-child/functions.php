<?php
// Add custom Theme Functions here
// Allow epub, mobi, azw or azw3 for digital downloads
add_filter('upload_mimes', function($mimetypes, $user)
{
    // Only allow these mimetypes for admins or shop managers
    $manager = $user ? user_can($user, 'manage_woocommerce') : current_user_can('manage_woocommerce');

    if ($manager)
    {
        $mimetypes = array_merge($mimetypes, [
            'epub' => 'application/octet-stream',
            'mobi' => 'application/octet-stream',
            'azw' => 'application/octet-stream',
            'azw3' => 'application/octet-stream'      
        ]);
    }

    return $mimetypes;
}, 10, 2);
//Remove SKU
function sv_remove_product_page_skus( $enabled ) {
    if ( ! is_admin() && is_product() ) {
        return false;
    }

    return $enabled;
}
add_filter( 'wc_product_sku_enabled', 'sv_remove_product_page_skus' );
/**
 * @snippet       Add privacy policy tick box at checkout
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=19854
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.3.4
 */
 
add_action( 'woocommerce_review_order_before_submit', 'bbloomer_add_checkout_privacy_policy', 9 );
   
function bbloomer_add_checkout_privacy_policy() {
  
woocommerce_form_field( 'privacy_policy', array(
    'type'          => 'checkbox',
    'class'         => array('form-row privacy'),
    'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
    'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
    'required'      => true,
    'label'         => 'I agree that my purchase is an eBook, not a physical book.',
)); 
  
}
  
// Show notice if customer does not tick
   
add_action( 'woocommerce_checkout_process', 'bbloomer_not_approved_privacy' );
  
function bbloomer_not_approved_privacy() {
    if ( ! (int) isset( $_POST['privacy_policy'] ) ) {
        wc_add_notice( __( 'Please check the agreement checkbox before proceeding' ), 'error' );
    }
}
function bb_remove_password_strength() {
	if ( wp_script_is( 'wc-password-strength-meter', 'enqueued' ) ) {
		wp_dequeue_script( 'wc-password-strength-meter' );
	}
}
add_action( 'wp_print_scripts', 'bb_remove_password_strength', 100 );


/** Disable Ajax Call from WooCommerce */
add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_cart_fragments', 11); 
function dequeue_woocommerce_cart_fragments() 
{ 
  if (is_front_page()) 
  {
    wp_dequeue_script('wc-cart-fragments'); 
  }   
}

add_action('wp_print_scripts', 'remove_password_strength_meter');
function remove_password_strength_meter() {
    // Deregister script about password strenght meter
    wp_dequeue_script('zxcvbn-async');
    wp_deregister_script('zxcvbn-async');
}
add_action( 'woocommerce_before_checkout_form', 'action_before_checkout_form' );
function action_before_checkout_form(){
    // HERE define the default payment gateway ID
    $default_payment_gateway_id = 'paypal_express';

    WC()->session->set('chosen_payment_method', $default_payment_gateway_id);
}

add_filter( 'woocommerce_billing_fields', 'wc_npr_filter_phone', 10, 1 );
function wc_npr_filter_phone( $address_fields ) {
 $address_fields['billing_phone']['required'] = true;
 return $address_fields;
}

?>
