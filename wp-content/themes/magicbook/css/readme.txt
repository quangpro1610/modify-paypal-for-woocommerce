When you set MAGICBOOK_DEBUG to true, then the theme will enqueue magicbook.css.
Turn off debug mode, magicbook.min.css is being used.

You can change MAGICBOOK_DEBUG in includes/class.magicbook.php