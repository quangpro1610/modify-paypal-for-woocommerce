<?php 
/* --------------------------
 * Footer
 ---------------------------*/
global $MB_VAN;
wp_footer();
echo '<div id="global_footer_text">'.wp_kses($MB_VAN['global_footer_text'],array(
				    'a' => array(
				        'href' => array(),
				        'title' => array(),
				        'class' => array(),
				        'rel' => array()
				    ),
				    'br' => array('class' => array()),
				    'em' => array('class' => array()),
				    'span' => array('class' => array()),
				    'strong' => array('class' => array()),
				    'img' => array(
				    	'class' => array(),
				    	'src' => array(),
				    	'alt' => array(),
				    	'style' => array(),
				    	'id' => array()
				    ),
				  )).'</div>';
?>

<!-- Say no to IE 8 ...  -->
<!--[if lt IE 9]>
	<div id="say-no-to-ie8"><h3>Sorry, we no longer support your browser anymore :(</h3><h5>Please update it to later version, just like:</h5><span><a target="_blank" href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">Newer IE</a></span>|<span><a target="_blank" href="https://www.mozilla.org/en-US/firefox/new/#">Firefox</a></span>|<span><a target="_blank" href="http://www.opera.com/computer/windows">Opera</a></span>|<span><a target="_blank" href="https://www.google.com/intl/en/chrome/browser/">Chrome</a></span></div>
<![endif]-->
</body>
</html>