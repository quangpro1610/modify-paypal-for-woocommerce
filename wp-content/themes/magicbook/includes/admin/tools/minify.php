<?php
$path = MAGICBOOK_INC_DIR.'admin/tools';
require_once $path . '/minify/src/Minify.php';
require_once $path . '/minify/src/CSS.php';
require_once $path . '/minify/src/JS.php';
require_once $path . '/minify/src/Exception.php';
require_once $path . '/minify/src/Exceptions/BasicException.php';
require_once $path . '/minify/src/Exceptions/FileImportException.php';
require_once $path . '/minify/src/Exceptions/IOException.php';
require_once $path . '/path-converter/src/ConverterInterface.php';
require_once $path . '/path-converter/src/Converter.php';

use MatthiasMullie\Minify;
if(isset($_GET['minify']) && $_GET['minify']=='css'){
	$CSSMinifier = new Minify\CSS();
	$CSSMinifier->add(MAGICBOOK_THEME_DIR.'/css/files/bookblock.css');
	$CSSMinifier->add(MAGICBOOK_THEME_DIR.'/css/files/component.css');
	$CSSMinifier->add(MAGICBOOK_THEME_DIR.'/css/files/perfect-scrollbar.min.css');
	$CSSMinifier->add(MAGICBOOK_THEME_DIR.'/css/files/reset.css');
	$CSSMinifier->add(MAGICBOOK_THEME_DIR.'/css/files/main.css');
	$CSSMinifier->add(MAGICBOOK_THEME_DIR.'/css/files/responsive.css');
	$CSSMinifier->minify(MAGICBOOK_THEME_DIR.'/css/magicbook.min.css');
}