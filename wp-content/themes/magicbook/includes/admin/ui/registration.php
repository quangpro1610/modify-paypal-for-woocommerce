<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
  exit( 'Direct script access denied.' );
}
if(isset($_POST['activate']) && $_POST['activate'] == true){
   $purchase_key = '';
   if(isset($_POST['purchase_key']) && $_POST['purchase_key'] !==''){
      $purchase_key = esc_html($_POST['purchase_key']);
   }else{
      $purchase_key = '';
      echo '<script>alert("Please enter a valid purchase code.");</script>';
   }
   MagicBookAdmin::activate_license($purchase_key);
}
if(isset($_POST['deactivate']) && $_POST['deactivate'] == true){
   MagicBookAdmin::deactivate_license(get_option('purchase_key'));
}
do_action('MagicBookAdmin_header');
?>
<div class="section">
      <?php if( !MagicBookAdmin::check_license()):?>
      <h3><?php esc_html_e('Please enter your purchase code to activate auto theme & bundled plugins updates.','magicbook');?></h3>
      <?php endif;?>

      <form id="#verify-envato-purchase" method="post" action="<?php echo admin_url('themes.php?page=magicbook-license');?>"> 
         <?php 
          settings_fields( 'magicbook_option_group' );
          do_settings_sections( 'magicbook_option_group' ); 
          echo MagicBookAdmin::text('purchase_key',get_option('purchase_key'), MagicBookAdmin::check_license());

          if( MagicBookAdmin::check_license()) {
            echo MagicBookAdmin::hidden('deactivate',true);
            submit_button('Deactivate','','deactivate_license','','');
            echo '<div class="activated">'.esc_html__('Your License Key is Activated!','magicbook').'</div>';
            echo '<span class="expired_date">The theme support will be expired at '.date("Y-m-d h:i:sa",strtotime(get_option('expired_date'))).'</span>';
          }else{
            echo MagicBookAdmin::hidden('activate',true);
            $button_text = 'Activate';
            submit_button($button_text,'primary','activate_license','','');
            if(get_option('expired_date')<>'' && MagicBookAdmin::is_expired()){
              echo '<div class="warning">'.__('Your theme support is expired, if you still need our support, please <a href="https://themeforest.net/item/magicbook-a-3d-flip-book-wordpress-theme/8808169?ref=themevan" target="_blank" style="color:#ffc107">Renew it</a>.','magicbook').'</div>';
            }else{
              echo '<div class="warning" id="no-activated" style="display:none;">'.esc_html__('Please enter a invalid purchase code.','magicbook').'</div>';
            }
          }
         ?>
      </form>
      <div class="box">
        <h3><?php esc_html_e('Where to get your purchase code?','magicbook');?></h3>
        <p><?php esc_html_e('Go to your Themeforest Download Page, click the green download button, then just select "License Certificate & Purchase Code" to download the license text file, you can find the purchase code in this file.','magicbook');?></p>
        <p><img src="<?php echo MAGICBOOK_INC_URI;?>admin/img/download.jpg" style="width:50%" /><img src="<?php echo MAGICBOOK_INC_URI;?>admin/img/purchase_code.jpg" style="width:50%" /></p>
      </div>
  </div>

</div>
