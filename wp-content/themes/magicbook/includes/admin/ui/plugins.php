<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
  exit( 'Direct script access denied.' );
}

do_action('MagicBookAdmin_header');

$plugins           = TGM_Plugin_Activation::$instance->plugins;
$installed_plugins = get_plugins();
?>

  <?php add_thickbox(); ?>
   <div class="box">
        <?php echo wp_kses_post(__( 'MagicBook Addon and WP Bakery Page Builder are bundled plugins. ', 'magicbook' ) ); ?>
  </div>

  <div class="section magicbook-admin-grid">
    <div id="the-list">
      <?php $magicbook_registered_plugins = magicbook_get_required_and_recommened_plugins(); ?>
      <?php foreach ( $plugins as $plugin ) : ?>
        <?php
        if ( ! array_key_exists( $plugin['slug'], $magicbook_registered_plugins ) ) {
          continue;
        }

        $class = '';
        $plugin_status = '';
        $file_path = $plugin['file_path'];
        $plugin_action = MagicBookAdmin::plugin_link( $plugin );

        // We have a repo plugin.
        if ( ! $plugin['version'] ) {
          $plugin['version'] = TGM_Plugin_Activation::$instance->does_plugin_have_update( $plugin['slug'] );
        }

        if ( is_plugin_active( $file_path ) ) {
          $plugin_status = 'active';
          $class = 'active';
        }
        ?>

        <div class="plugin-card <?php echo esc_attr( $class ); ?>">
            <div class="plugin-card-top">
              <?php if ( isset( $plugin_action['update'] ) && $plugin_action['update'] ) : ?>
              <div class="update-message notice inline notice-warning notice-alt arovane-plugin-update-notice">
                <p><?php printf( esc_attr__( 'New Version Available: %s', 'magicbook' ), esc_attr( $plugin['version'] ) ); ?></p>
              </div>
              <?php endif; ?>

              <?php if ( isset( $plugin['required'] ) && $plugin['required'] ) : ?>
              <div class="plugin-required">
                <?php esc_html_e( 'Required', 'magicbook' ); ?>
              </div>
              <?php endif; ?>
              <div class="name column-name">
                <h3>
                  <?php if ( 'active' == $plugin_status ) : ?>
                    <span><?php printf( esc_attr__( 'Active: %s', 'magicbook' ), esc_attr( $plugin['name'] ) ); ?></span>
                  <?php else : ?>
                    <?php echo esc_attr( $plugin['name'] ); ?>
                  <?php endif; ?>
                  <img src="<?php echo esc_url_raw( $plugin['image_url'] ); ?>" class="plugin-icon" alt="">
                </h3>
              </div>

              <div class="action-links">
                <ul class="plugin-action-buttons">
                 <?php 
                  foreach ( $plugin_action as $action ) : 
                    echo $action;
                  endforeach; 
                 ?>
                </ul>        
              </div>

              <div class="desc column-description">
                <p><?php echo esc_attr( $plugin['desc'] ); ?></p>
              </div>

            </div>
        </div>
      <?php endforeach; ?>
      <div class="clear"></div>

    </div>
  </div>

</div>