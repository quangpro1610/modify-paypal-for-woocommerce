<?php

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
  exit( 'Direct script access denied.' );
}

do_action('MagicBookAdmin_header');

$allow_tags = array(
    //formatting
    'strong' => array(),
    'em'     => array(),
    'b'      => array(),
    'i'      => array(),

    //links
    'a'     => array(
        'href' => array()
    )
);
?>
<div class="section">
 <div class="box">
    <h2><?php esc_html_e( 'FAQ', 'magicbook' ); ?></h2>
    <div class="questions">
      <div class="item">
        <h4><?php esc_html_e("1. Why I can't import the sample data?",'magicbook');?></h4>
        <p><?php esc_html_e("You'd better deactivated all plugins except WooCommerce before you import the sample data. After the data is imported, then reactivate the plugins.","magicbook");?></p>
        </div>
      
      <div class="item">
        <h4><?php esc_html_e("2. How I upgrade the bundled plugins?",'magicbook');?></h4>
        <p><?php esc_html_e("You can upgrade them in MagicBook > Plugins page after you upgrade the new version of MagicBook, because these bundled premium plugins will come with the new version of this theme.","magicbook");?></p>
      </div>
    
      <div class="item">
        <h4><?php esc_html_e("3. What's the safe way to customize the theme without lose my changes when I upgrade the theme in the future?",'magicbook');?></h4>
        <p><?php esc_html_e("First of all, We strongly suggest you don't modify the MagicBook theme, otherwise, you will not able to upgrade the theme smoothly in the future. ",'magicbook');?> </p>
        <p><?php printf(wp_kses(__("The best way is use child theme, you can put all your custom changes into child theme folder. Learn more about the <a href='%s' target='_blank'>Child Theme in WordPress Codex</a>. ",'magicbook'),$allow_tags),'https://codex.wordpress.org/Child_Themes');?></p>
        
        <p>
        <?php printf(wp_kses(__("If you don't want to create child theme, you can put the custom CSS and JS into 'Appearance > Customizer > Custom Code', or install <a href='%s' target='_blank'>Custom css-js-php plugin</a>, it allows you to insert your own custom CSS, JS and PHP functions through your WordPress backend.  ",'magicbook'),$allow_tags),'https://wordpress.org/plugins/custom-css-js-php/');?>
        </p>
      </div>
    </div>
  </div>
</div>

</div>