<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
  exit( 'Direct script access denied.' );
}

do_action('MagicBookAdmin_header');

$allow_tags = array(
    //formatting
    'strong' => array(),
    'em'     => array(),
    'b'      => array(),
    'i'      => array(),

    //links
    'a'     => array(
        'href' => array()
    )
);
?>
    <div class="section">
    	  <div class="box">
            <h3><?php esc_html_e( 'Only 3 Steps For MagicBook Theme Setup', 'magicbook'); ?></h3>
            <ol>
                <li><?php printf(wp_kses(__('First of all, please add a valid purchase code in the <a href="%s" target="_blank">"Registration"</a> page to activate the theme auto update and get the permission to install the bundled premium plugins.','magicbook'),$allow_tags),admin_url('themes.php?page=magicbook-license'));?></li>

    			<li><?php printf(wp_kses(__('Please activate all the required plugins through <a href="%s" target="_blank">MagicBook Plugins Installer</a>. After you activated MagicBook Addon plugin, you will see the "Demos" tab in the above menu.','magicbook'),$allow_tags),admin_url('themes.php?page=magicbook-plugins'));?></li>
    		
        		<li>
        		<?php printf(wp_kses(__('You can one click import all demo content through <a href="%s" target="_blank">Demos</a> page.','magicbook'),$allow_tags), admin_url('themes.php?page=magicbook-demos'));?>
        		</li>
            </ol>
            <p>                       
             <?php printf(wp_kses(__('Additionally, we integrate the theme options into <a href="%s" target="_blank">Customize</a> that allow you to change the font, color scheme, header layout and some other features.
    ','magicbook'),$allow_tags), admin_url('customize.php'));?>
            </p>
          </div>
    </div>

</div>