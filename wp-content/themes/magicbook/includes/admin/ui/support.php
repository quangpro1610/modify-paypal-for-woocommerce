<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
  exit( 'Direct script access denied.' );
}

do_action('MagicBookAdmin_header');
?>

 <div class="section">
        <div class="box">
        <p><?php esc_html_e('MagicBook comes with 6 months of free support for every license you purchase. Support can be extended through subscriptions via ThemeForest. All support for MagicBook is handled through our support center on our support forum. Please note, we don\'t provide customization service, so the free support only includes bug fixing, trouble using and small changes.','magicbook');?></p>
        <a class="button button-primary" href="http://envato.themevan.com/forum/magicbook" target="_blank">Go to Support Forum</a>
      </div>

      <div class="box">
        <h3><?php esc_html_e( 'Documentations', 'magicbook' ); ?></h3>
        <ul>
          <li><a href="http://themevan.com/docs/magicbook" target="_blank">- <?php esc_html_e('MagicBook Documentation','magicbook');?></a></li>
          <li><a href="https://wpbakery.atlassian.net/wiki/display/VC/Visual+Composer+Pagebuilder+for+WordPress" target="_blank">- <?php esc_html_e('Visual Composer Documentation','magicbook');?></a></li>
          <li><a href="https://www.themepunch.com/revslider-doc/slider-revolution-documentation/" target="_blank">- <?php esc_html_e('Revolution Slider Documentation','magicbook');?></a></li>
        </ul>
      </div>
 </div>
</div>