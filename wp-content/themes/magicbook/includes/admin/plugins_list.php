<?php
/**
 * Plugins for TGM usage.
 * @package   MagicBook
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
require_once MAGICBOOK_INC_DIR . 'admin/class-tgm-plugin-activation.php';

/**
 * Gets all recommended and required plugins for use in TGM plugin.
 *
 * @since 5.1.6
 */
function magicbook_get_required_and_recommened_plugins() {


	$is_plugins_page = false;
	if ( ( isset( $_GET['page'] ) && 'magicbook-plugins' === $_GET['page'] ) ||
		 ( isset( $_GET['page'] ) && 'install-required-plugins' === $_GET['page'] ) ||
		 ( isset( $_SERVER['HTTP_REFERER'] ) && false !== strpos( esc_url_raw( wp_unslash( $_SERVER['HTTP_REFERER'] ) ), 'HTTP_REFERER' ) )
	) {
		$is_plugins_page = true;
	}

	// Get the bundled plugin information.
	$bundled_plugins = MagicBook()->get_bundled_plugins();

	/**
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$key_url_md5 = 'Wswb86sr07^d1a7y#';
	$plugins = array(
		$bundled_plugins['magicbook_addon']['slug'] => array(
			'name'               => $bundled_plugins['magicbook_addon']['name'],
			'slug'               => $bundled_plugins['magicbook_addon']['slug'],
			'source'             => 'https://www.themevan.com/download.php?'.tmv_encrypt_url('purchase_key='.get_option('purchase_key').'&item_id='.MAGICBOOK_ITEM_ID.'&file='.$bundled_plugins['magicbook_addon']['slug'], $key_url_md5),
			'desc'      => esc_html__('MagicBook Addon provides advanced options, shortcodes, PHP class, methods and so on.','magicbook'),
			'required'           => true,
			'version'            => $bundled_plugins['magicbook_addon']['version'],
			'force_activation'   => false,
			'force_deactivation' => false,
			'external_url'       => '',
			'image_url'          => MAGICBOOK_INC_URI.'admin/img/magicbook_addon.png',
		),
		$bundled_plugins['visual_composer']['slug'] => array(
			'name'               => $bundled_plugins['visual_composer']['name'],
			'slug'               => $bundled_plugins['visual_composer']['slug'],
			'source'             => 'https://www.themevan.com/download.php?'.tmv_encrypt_url('purchase_key='.get_option('purchase_key').'&item_id='.MAGICBOOK_ITEM_ID.'&file='.$bundled_plugins['visual_composer']['slug'], $key_url_md5),
			'desc'      		 => esc_html__('The most popular and powerful page builder.','magicbook'),
			'required'           => true,
			'version'            => $bundled_plugins['visual_composer']['version'],
			'force_activation'   => false,
			'force_deactivation' => false,
			'external_url'       => '',
			'image_url'          => MAGICBOOK_INC_URI.'admin/img/visual_composer.png',
		),
		'contact-form-7' => array(
			'name'      => 'Contact Form 7',
			'slug'      => 'contact-form-7',
			'required'  => false,
			'desc'      => esc_html__('Just another contact form plugin. Simple but flexible.','magicbook'),
			'image_url' => MAGICBOOK_INC_URI.'admin/img/cf7.png',
		),
		
	);

	return $plugins;
}

/**
 * Require the installation of any required and/or recommended third-party plugins here.
 * See http://tgmpluginactivation.com/ for more details
 */
function magicbook_register_required_and_recommended_plugins() {

	// Get all required and recommended plugins.
	$plugins = magicbook_get_required_and_recommened_plugins();

	// Change this to your theme text domain, used for internationalising strings.
	$theme_text_domain = 'magicbook';

	/**
	 * Array of configuration settings. Amend each line as needed.
	 * If you want the default strings to be available under your own theme domain,
	 * leave the strings uncommented.
	 * Some of the strings are added into a sprintf, so see the comments at the
	 * end of each line for what each argument will be.
	 */
	$config = array(
		'domain'        	=> $theme_text_domain,
		'default_path'  	=> '',
		'parent_slug' 		=> 'magicbook',
		'menu'            	=> 'magicbook-plugins',
		'has_notices'     	=> true,
		'is_automatic'    	=> true,
		'message'         	=> '',
	);

	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'magicbook_register_required_and_recommended_plugins' );

/**
 * Returns the user capability for showing the notices.
 *
 * @return string
 */
function magicbook_tgm_show_admin_notice_capability() {
	return 'edit_theme_options';
}
add_filter( 'tgmpa_show_admin_notice_capability', 'magicbook_tgm_show_admin_notice_capability' );

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
