<?php
/**
 * Welcome Screen Class
 * Sets up the welcome screen page, hides the menu item
 * and contains the screen content.
 */
class MagicBookAdmin {

	/**
	 * Constructor
	 * Sets up the welcome screen
	 */
	public function __construct() {

		add_action( 'admin_menu', array( $this, 'admin_register_menu' ) );
		add_action( 'load-themes.php', array( $this, 'activation_redirection' ) );
		add_action( 'load-themes.php', array( $this, 'admin_notice' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_style' ) );
		add_action( 'admin_init', array($this,'register_option_setting'));
		add_action( 'admin_init', array($this,'plugin_action'));
		add_action( 'MagicBookAdmin_header', array( $this, 'admin_ui_header' ), 10 );
		add_action( 'demo_importer_header', array( $this, 'admin_ui_header' ), 10);
		add_filter( 'pt-ocdi/plugin_page_setup', array($this, 'demo_import_menu'));
		add_filter( 'pt-ocdi/plugin_page_title', array($this,'remove_demo_page_title'));
		add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );
		add_action( 'demo_importer_before', array($this, 'no_registered_info_on_demo_page'));
		add_action( 'wp_dashboard_setup', array($this,'add_dashboard_widgets') );
		
		if(get_option('magicbook_is_registered')==''){
		   add_option( 'magicbook_is_registered', false, '', 'yes' );
	    }
	} // end constructor
     

	/**
	 * Adds an admin notice upon successful activation.
	 * @since 1.0.0
	 */
	public function activation_redirection() {
		global $pagenow;

		if ( is_admin() && 'themes.php' == $pagenow && isset( $_GET['activated'] ) ) { 
			wp_redirect(admin_url("admin.php?page=magicbook")); 
		}
	}

	/**
	 * Display an admin notice linking to the registration screen
	 * @since 1.0.0
	 */
	public function admin_notice() {
		if( !MagicBookAdmin::check_license()) {
		?>
			<div class="updated notice is-dismissible">
				<p><?php echo sprintf( esc_html__( 'Thanks for choosing MagicBook! Please activate your purchase code before you install the required plugins and demos.', 'magicbook' ), '<a href="' . esc_url( admin_url( 'admin.php?page=magicbook-license' ) ) . '">', '</a>' ); ?></p>
				<p><a href="<?php echo esc_url( admin_url( 'admin.php?page=magicbook-license' ) ); ?>" class="button" style="text-decoration: none;"><?php esc_html_e( 'Register Now', 'magicbook' ); ?></a></p>
			</div>
		<?php
	    }
	}

	/**
	 * Load admin screen css
	 * @return void
	 * @since  1.0.0
	 */
	public function admin_style( $hook_suffix ) {
			wp_enqueue_style( 'magicbook-dashboard', MAGICBOOK_INC_URI . 'admin/css/dashboard.css','');
			wp_enqueue_style( 'thickbox' );
			wp_enqueue_script('thickbox' );
	}

	/**
	 * Creates the dashboard page
	 * @see  add_theme_page()
	 * @since 1.1.5
	 */
	public function admin_register_menu() {
		if(current_user_can('edit_theme_options')){
			$plugins_callback = array( $this, 'ui_plugins' );
			if ( isset( $_GET['tgmpa-install'] ) || isset( $_GET['tgmpa-update'] ) ) {
				require_once  MAGICBOOK_INC_DIR.'admin/class-tgm-plugin-activation.php';
				remove_action( 'admin_notices', array( $GLOBALS['tgmpa'], 'notices' ) );
				$plugins_callback = array( $GLOBALS['tgmpa'], 'install_plugins_page' );
			}

			add_theme_page( esc_html__('Registration','magicbook'), esc_html__('Registration','magicbook'), 'manage_options', 'magicbook-license', array( $this, 'ui_registration' ));
			add_theme_page(esc_html__('Getting Started','magicbook'), esc_html__('Getting Started','magicbook'), 'manage_options', 'magicbook', array( $this, 'ui_getting_started' ));
			add_theme_page( esc_html__('Plugins','magicbook'), esc_html__('Plugins','magicbook'), 'install_plugins', 'magicbook-plugins', $plugins_callback);
			add_theme_page(esc_html__('Support','magicbook'), esc_html__('Support','magicbook'), 'edit_posts', 'magicbook-support', array( $this, 'ui_support' ));
			add_theme_page(esc_html__('FAQ','magicbook'), esc_html__('FAQ','magicbook'), 'edit_posts', 'magicbook-faq', array( $this, 'ui_faq' ));
			
	    }
	}


	/**
	 * Demo Importer Menu
	 * @since 1.1.5
	 */
	public function demo_import_menu( $default_settings ) {
		$default_settings['page_title']  = esc_html__( 'MagicBook Demos' , 'magicbook' );
		$default_settings['menu_title']  = esc_html__( 'Demos' , 'magicbook' );
		$default_settings['capability']  = 'import';
		$default_settings['menu_slug']   = 'magicbook-demos';

		return $default_settings;
	}

	/**
	 * Remove Demo Page Title
	 */
	public function remove_demo_page_title(){
		return '';
	}

	public function no_registered_info_on_demo_page(){
		if( !MagicBookAdmin::check_license()) {
			echo '<div class="box" style="border-left: 4px solid #dc3232;">'.esc_html__('Purchase Code is required before you import the demo data, please add a valid purchase code in Registration page.','magicbook').'</div>';
			die();
		}
	}

	/**
	 * The Admin screen header
	 * @since 1.0.0
	 */
	public function admin_ui_header() {
		$magicbook = wp_get_theme( 'magicbook' );
		?>
		<div class="wrap about-wrap magicbook-wrap">
			<h1><?php echo '<strong>'.esc_attr($magicbook['Name']).'</strong> <sup class="version">' . esc_attr( $magicbook['Version'] ) . '</sup>'; ?></h1>
			<p class="intro"><?php echo $magicbook['Description'];?></p>

		    <?php
			  $active_tab = '';
			  if( isset( $_GET[ 'page' ] ) ) {
			      $active_tab = $_GET[ 'page' ];
			  }
		    ?>
		    <h2 class="nav-tab-wrapper">
				<a href="<?php echo esc_url(admin_url('admin.php?page=magicbook-license'));?>" class="<?php echo $active_tab == 'magicbook-license' ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_html_e('Registration','magicbook');?></a>
				<a href="<?php echo esc_url(admin_url('admin.php?page=magicbook'));?>" class="<?php echo $active_tab == 'magicbook' ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_html_e('Getting Started','magicbook');?></a>
				<a href="<?php echo esc_url(admin_url('admin.php?page=magicbook-plugins'));?>" class="<?php echo $active_tab == 'magicbook-plugins' ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_html_e('Plugins','magicbook');?></a>
				<?php if(class_exists('MagicBookAddon')):?>
				<a href="<?php echo esc_url(admin_url('themes.php?page=magicbook-demos'));?>" class="<?php echo $active_tab == 'magicbook-demos' ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_html_e('Demos','magicbook');?></a>
			    <?php endif;?>
				<a href="<?php echo esc_url(admin_url('admin.php?page=magicbook-support'));?>" class="<?php echo $active_tab == 'magicbook-support' ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_html_e('Support','magicbook');?></a>
				<a href="<?php echo esc_url(admin_url('admin.php?page=magicbook-faq'));?>" class="<?php echo $active_tab == 'magicbook-faq' ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_html_e('FAQ','magicbook');?></a>
			</h2>
		<?php
	}

	/**
	 * Admin screen UI
	 * @since 1.0.0
	 */
	public function ui_getting_started(){
	   require_once( MAGICBOOK_INC_DIR . 'admin/ui/getting-started.php' );
	}
    public function ui_registration(){
	   require_once( MAGICBOOK_INC_DIR . 'admin/ui/registration.php' );
    }
    public function ui_plugins(){
	   require_once( MAGICBOOK_INC_DIR . 'admin/ui/plugins.php' );
    }
    public function ui_support(){
	   require_once( MAGICBOOK_INC_DIR . 'admin/ui/support.php' );
    }
    public function ui_faq(){
	   require_once( MAGICBOOK_INC_DIR . 'admin/ui/faq.php' );
    }

	/** 
     * Check license key
     * @since 1.0.0 
     */
	public static function check_license(){
     	$license = get_option('purchase_key');
		if($license <> '' && isset($license)){
			return true;
		}else{
			return false;
		}
	}

	/* check license */
	public static function activate_license($license_key) {
		$request = wp_remote_get(THEMEVAN_API.'envato_verifier.php?purchase_key='.$license_key.'&item_id='.MAGICBOOK_ITEM_ID);
	    if( is_wp_error( $request ) ) {
	      return false; // Bail early
	    }
	    $oauth_api = wp_remote_retrieve_body( $request );
	    $oauth = json_decode($oauth_api,TRUE);
		$status = $oauth['status'];

		if( $status == 'activated') {
			update_option('purchase_key', $license_key);
			update_option('expired_date', $oauth['supported_until']);
			return true;
			exit;
			// this license is still valid
		} else {
			echo '<script>jQuery(document).ready(function($){$("#no-activated").show();});</script>';
			self::deactivate_license();
			return false;
			exit;
			// this license is no longer valid
		}
	}

	/** 
     * Dectivate license key
     * @since 1.0.3
     */
	public static function deactivate_license(){
		update_option('purchase_key', '');
		update_option('expired_date', '');
	}

	/**
	 * Check license Expired
	 * @since 1.2.5
	 */
	public static function is_expired(){
		if(date("Y-m-d h:i:sa") >= date("Y-m-d h:i:sa",strtotime(get_option('expired_date')))){
             return true;
        }else{
        	 return false;
        }
        return true;
	}

	/**
	 * Register Option Setting
	 * @since 1.0.0
	 */
	public function register_option_setting() {
	   register_setting( 'magicbook_option_group', 'purchase_key', ''); 
	   register_setting( 'magicbook_option_group', 'expired_date', ''); 
    } 
    

    /**
     * Text Field
     */
	public static function text($name,$val,$readonly) {
	  if(null !== get_option($name) && get_option($name)<>''){
	  	 $val = get_option($name);
	  }
	  $readonly_prop = '';
	  if($readonly==true){
	  	 $readonly_prop = 'readonly';
	  }
	  $return_html = '<input name="'.$name.'" type="text" class="ui textfield" value="'.$val.'" '.$readonly_prop.'>';      
      return $return_html;
	}

	/**
     * Hidden Field
     */
	public static function hidden($name,$val) {
	  if(null !== get_option($name) && get_option($name)<>''){
	  	 $val = get_option($name);
	  }
	  $return_html = '<input name="'.$name.'" type="hidden" class="ui textfield" value="'.$val.'">';      
      return $return_html;
	}
   
    /**
     * Add Dashboard Widget
     */
    public function add_dashboard_widgets() {
        wp_add_dashboard_widget('dashboard_widget', 'ThemeVan News', array($this, 'dashboard_widget')); 
    }

    public function dashboard_widget() {
	   echo '<div class="themevan-news-widget rss-widget">';
		wp_widget_rss_output(array(
		'url' => 'https://www.themevan.com/category/themevan-news/feed',
		'items' => 5,
		'show_summary' => 0,
		'show_author' => 0,
		'show_date' => 1
		));
		echo "</div>";
    }

	/**
	 * Get the plugin link.
	 *
	 * @access  public
	 * @param array $item The plugin in question.
	 * @return  array
	 */
	public static function plugin_link( $item ) {
		$installed_plugins = get_plugins();

		$item['sanitized_plugin'] = $item['name'];

		$actions = array();

		// We have a repo plugin.
		if ( ! $item['version'] ) {
			$item['version'] = TGM_Plugin_Activation::$instance->does_plugin_have_update( $item['slug'] );
		}

		$disable_class = '';
		$data_version  = '';
		if ( 'revslider' == $item['slug'] || 'js_composer' == $item['slug'] || 'themevan-shortcodes' == $item['slug']){
			if(!MagicBookAdmin::check_license()){
				$disable_class = ' disabled';
			}
		}

		// We need to display the 'Install' hover link.
		if ( ! isset( $installed_plugins[ $item['file_path'] ] ) ) {
			if ( ! $disable_class ) {
				$url = esc_url( wp_nonce_url(
					add_query_arg(
						array(
							'page'          => rawurlencode( TGM_Plugin_Activation::$instance->menu ),
							'plugin'        => rawurlencode( $item['slug'] ),
							'plugin_name'   => rawurlencode( $item['sanitized_plugin'] ),
							'tgmpa-install' => 'install-plugin',
							'return_url'    => 'magicbook_plugins',
						),
						TGM_Plugin_Activation::$instance->get_tgmpa_url()
					),
					'tgmpa-install',
					'tgmpa-nonce'
				) );
			} else {
				$url = '#';
			}
			$actions = array(
				'install' => '<a href="' . $url . '" class="button button-primary' . $disable_class . '"' . $data_version . ' title="' . sprintf( esc_attr__( 'Install %s', 'magicbook' ), $item['sanitized_plugin'] ) . '">' . esc_attr__( 'Install', 'magicbook' ) . '</a>',
			);
		} elseif ( is_plugin_inactive( $item['file_path'] ) ) {
			// We need to display the 'Activate' hover link.
			$url = esc_url( add_query_arg(
				array(
					'plugin'               => rawurlencode( $item['slug'] ),
					'plugin_name'          => rawurlencode( $item['sanitized_plugin'] ),
					'magicbook-activate'       => 'activate-plugin',
					'magicbook-activate-nonce' => wp_create_nonce( 'magicbook-activate' ),
				),
				admin_url( 'admin.php?page=magicbook-plugins' )
			) );

			$actions = array(
				'activate' => '<a href="' . $url . '" class="button button-primary"' . $data_version . ' title="' . sprintf( esc_attr__( 'Activate %s', 'magicbook' ), $item['sanitized_plugin'] ) . '">' . esc_attr__( 'Activate' , 'magicbook' ) . '</a>',
			);
		} elseif ( version_compare( $installed_plugins[ $item['file_path'] ]['Version'], $item['version'], '<' ) ) {
			$disable_class = '';
			// We need to display the 'Update' hover link.
			$url = wp_nonce_url(
				add_query_arg(
					array(
						'page'          => rawurlencode( TGM_Plugin_Activation::$instance->menu ),
						'plugin'        => rawurlencode( $item['slug'] ),
						'tgmpa-update'  => 'update-plugin',
						'version'       => rawurlencode( $item['version'] ),
						'return_url'    => 'magicbook_plugins',
					),
					TGM_Plugin_Activation::$instance->get_tgmpa_url()
				),
				'tgmpa-update',
				'tgmpa-nonce'
			);
			if ('revslider' == $item['slug'] || 'js_composer' == $item['slug'] || 'themevan_shortcodes' == $item['slug']){
				if( ! MagicBookAdmin::check_license() ){
				   $disable_class = ' disabled';
			    }
			}
			$actions = array(
				'update' => '<a href="' . $url . '" class="button button-primary' . $disable_class . '" title="' . sprintf( esc_attr__( 'Update %s', 'magicbook' ), $item['sanitized_plugin'] ) . '">' . esc_attr__( 'Update', 'magicbook' ) . '</a>',
			);
		} elseif ( is_plugin_active( $item['file_path'] ) ) {
			$url = esc_url( add_query_arg(
				array(
					'plugin'                 => rawurlencode( $item['slug'] ),
					'plugin_name'            => rawurlencode( $item['sanitized_plugin'] ),
					'magicbook-deactivate'       => 'deactivate-plugin',
					'magicbook-deactivate-nonce' => wp_create_nonce( 'magicbook-deactivate' ),
				),
				admin_url( 'admin.php?page=magicbook-plugins' )
			) );
			$actions = array(
				'deactivate' => '<a href="' . $url . '" class="button button-primary" title="' . sprintf( esc_attr__( 'Deactivate %s', 'magicbook' ), $item['sanitized_plugin'] ) . '">' . esc_attr__( 'Deactivate', 'magicbook' ) . '</a>',
			);
		} // End if.

		return $actions;
	}

	/**
	 * Actions to run on initial theme activation.
	 */
	public function plugin_action() {

		if ( current_user_can( 'edit_theme_options' ) ) {

			if ( isset( $_GET['magicbook-deactivate'] ) && 'deactivate-plugin' === $_GET['magicbook-deactivate'] ) {
				check_admin_referer( 'magicbook-deactivate', 'magicbook-deactivate-nonce' );

				$plugins = TGM_Plugin_Activation::$instance->plugins;

				foreach ( $plugins as $plugin ) {
					if ( isset( $_GET['plugin'] ) && $plugin['slug'] == $_GET['plugin'] ) {
						deactivate_plugins( $plugin['file_path'] );
					}
				}
			}
			if ( isset( $_GET['magicbook-activate'] ) && 'activate-plugin' === $_GET['magicbook-activate'] ) {
				check_admin_referer( 'magicbook-activate', 'magicbook-activate-nonce' );

				$plugins = TGM_Plugin_Activation::$instance->plugins;

				foreach ( $plugins as $plugin ) {
					if ( isset( $_GET['plugin'] ) && $plugin['slug'] == $_GET['plugin'] ) {
						activate_plugin( $plugin['file_path'] );
						wp_safe_redirect( admin_url( 'admin.php?page=magicbook-plugins' ) );
						exit;
					}
				}
			}
		}
	}

	

}

$GLOBALS['MagicBookAdmin'] = new MagicBookAdmin();