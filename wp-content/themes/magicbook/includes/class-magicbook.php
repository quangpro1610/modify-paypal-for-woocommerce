<?php
/**
 * magicbook Initialize
 * @package magicbook
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if(!class_exists('magicbook')){
	class magicbook{
		/**
		 * A reference to an instance of this class.
		 *
		 * @since 1.0.0
		 * @var   object
		 */
		private static $instance = null;

		/**
		 * Bundled Plugins.
		 *
		 * @static
		 * @access public
		 * @var array
		 */
		public static $bundled_plugins = array(
			'magicbook_addon' => array(
				'slug'    => 'magicbook-addon',
				'name'    => 'MagicBook Addon',
				'version' => '1.0.8.3',
			),
			'visual_composer' => array(
				'slug'    => 'js_composer',
				'name'    => 'WPBakery Page Builder',
				'version' => '5.5.2',
			),
		);

		/**
		 * Setup class.
		 * @since 1.0
		 */
		public function __construct() {
			add_action( 'after_setup_theme',          array( $this, 'constants' ) , 1);
			add_action( 'after_setup_theme',          array( $this, 'setup' ), 2);
			add_action( 'after_setup_theme',          array( $this, 'load_files' ), 3);
			add_action( 'after_setup_theme',          array( $this, 'check_update' ), 4);
			add_action( 'after_setup_theme',          array( $this, 'example_content' ) );
			add_action( 'wp_enqueue_scripts',         array( $this, 'scripts' ),       10 );
			add_action( 'wp_enqueue_scripts',         array( $this, 'child_scripts' ), 30 ); 
			add_action( 'admin_init',         		  array( $this, 'admin_scripts' ), 10);
		}

		/**
		 * Define Constants for the theme.
		 * @since 1.0
		 */
		public function constants(){
			define('MAGICBOOK_THEME_URI', get_template_directory_uri());
			define('MAGICBOOK_THEME_DIR', get_template_directory());
			define('MAGICBOOK_INC_URI', MAGICBOOK_THEME_URI.'/includes/');
			define('MAGICBOOK_INC_DIR', MAGICBOOK_THEME_DIR.'/includes/');
			define('MAGICBOOK_DEBUG',   FALSE);
			define('THEMEVAN_API', 'https://www.themevan.com/api/');
			define('MAGICBOOK_ITEM_ID', intval('8808169')); // Don't change it, otherwise, your purchase code can't be verified.
		}

		/**
		 * Gets the bundled plugins.
		 *
		 * @static
		 * @access public
		 * @since 5.0
		 * @return array Array of bundled plugins.
		 */
		public static function get_bundled_plugins() {
			return self::$bundled_plugins;
		}

		/**
		 * Sets up theme defaults and registers support for various WordPress features.
		 * @since 1.0
		 * Note that this function is hooked into the after_setup_theme hook, which
		 * runs before the init hook. The init hook is too late for some features, such
		 * as indicating support for post thumbnails.
		 */
		public function setup() {
			global $content_width;

			/*
		 	 * Make theme available for translation.
			 * Translations can be filed in the /languages/ directory.
			 * If you're building a theme based on magicbook, use a find and replace
			 * to change 'magicbook' to the name of your theme in all the template files.
			 */
			load_theme_textdomain( 'magicbook', get_template_directory() . '/languages' );
			$locale = get_locale(); 
			$locale_file = get_template_directory_uri()."/languages/$locale.php"; 
			if ( is_readable($locale_file) ) require_once($locale_file);

			// Add default posts and comments RSS feed links to head.
			add_theme_support( 'automatic-feed-links' );

			/*
			 * Let WordPress manage the document title.
			 * By adding theme support, we declare that this theme does not use a
			 * hard-coded <title> tag in the document head, and expect WordPress to
			 * provide it for us.
			 */
			add_theme_support( 'title-tag' );
			
			/* Enable the editor style */
			add_editor_style('editor-style.css');
			
			/* Declare WooCommerce support*/
			add_theme_support( 'woocommerce' );
			add_theme_support( 'wc-product-gallery-zoom' );
			add_theme_support( 'wc-product-gallery-lightbox' );
			add_theme_support( 'wc-product-gallery-slider' );

			/*Remove the default gallery style*/
			add_filter( 'use_default_gallery_style', '__return_false' );

			/*Change excerpt more string*/
			function magicbook_excerpt_more( $more ) {
				return '...';
			}
			add_filter( 'excerpt_more', 'magicbook_excerpt_more' );

			/*Set the default layout to two columns*/
			add_filter( 'default_content', 'magicbook_editor_content', 10, 2 );
			function magicbook_editor_content( $content, $post ) {
			    switch( $post->post_type ) {
			        case 'post':
			            $content = '';
			        break;
			        case 'page':
			            $content = '[vc_row][vc_column width="1/2"][/vc_column][vc_column width="1/2"][/vc_column][/vc_row]';
			        break;
			    }
			    return $content;
			}

			/*
			 * Enable support for Post Thumbnails on posts and pages.
			 *
			 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
			 */
			add_theme_support( 'post-thumbnails' );
			if ( function_exists( 'add_image_size')){  
				add_image_size('portfolio_thumbnail', 800, 800,true);
			}

			// This theme uses wp_nav_menu() in one location.
			register_nav_menus(array('primary_navi' => 'Primary Menu'));

			/*
			 * Switch default core markup for search form, comment form, and comments
			 * to output valid HTML5.
			 */
			add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			) );

			// Sets the content width in pixels, based on the theme's design and stylesheet.
			if ( ! isset( $content_width ) ) {
				$content_width = apply_filters( 'magicbook_content_width', 1000 );
			}
		}

		

		/**
		 * Generate Example Page
		 */
		public function example_content(){

			/*Add the sample page.*/
			if (isset($_GET['activated']) && is_admin()){
			        wp_delete_post(2,true);

			        $new_page_title = 'Example Page';
			        $new_page_content = '[vc_row][vc_column width="1/2"][vc_text_separator title="Instruction " title_align="separator_align_center" color="grey" style="double"][vc_column_text css_animation="bottom-to-top"]Hi,friend!

			Thanks for you are using MagicBook theme! This is a sample page which will tell you how to create a book page like the demo does.

			- When you edit this page, you will see the Visual Composer page builder interface, there are two columns represent respectively the left side and the right side of the book page, then, just click "+" button to add the elements into the book page which you want.

			- Go to Appearance > Menus, create your own custom menu and set the "Theme locations" to "Primary Menu" . That\'s it.

			You can delete this page and create your own pages after you got it.

			Finally, hope you will enjoy in MagicBook Theme!
			[/vc_column_text][/vc_column][vc_column width="1/2"][vc_text_separator title="Recent Blog" title_align="separator_align_center" color="grey" style="double"][van_blog posts_per_page="5"][/vc_column][/vc_row]'; 
					
				    $page_check = get_page_by_title($new_page_title);
			        $new_page = array(
					        'post_id' => '2',
			                'post_type' => 'page',
			                'post_title' => $new_page_title,
			                'post_content' => $new_page_content,
			                'post_status' => 'publish',
			                'post_author' => 1,
			        );
			        if(!isset($page_check->ID)){
			                $new_page_id = wp_insert_post($new_page);				
			        }
			}
		}

		/**
		 * Load Google Fonts
		 * Hook: magicbook_scripts
		 */
		public function scripts() {

			/* Fonts */
		    $fonts_url = '';
		    $tulpen_one = _x( 'on', 'Tulpen One font: on or off', 'magicbook' );
		    $roboto = _x( 'on', 'Roboto: on or off', 'magicbook' );
		    $arimo = _x( 'on', 'Arimo: on or off', 'magicbook' );
		    $raleway = _x( 'on', 'Raleway: on or off', 'magicbook' );
		    $marvel = _x( 'on', 'Marvel: on or off', 'magicbook' );
		    
		    if ('off' !== $tulpen_one || 'off' !== $roboto || 'off' !== $arimo || 'off' !== $raleway || 'off' !== $marvel) {
		            $font_families = array();
		         
		            if ( 'off' !== $roboto ) {
		                $font_families[] = 'Roboto:100,300,400,400italic,500,500italic,700,700italic,900,900italic';
		            }
		            
		            if ( 'off' !== $tulpen_one ) {
		                $font_families[] = 'Tulpen One';
		            }
		            
		            if ( 'off' !== $arimo ) {
		                $font_families[] = 'Arimo:400,700';
		            }

		            if ( 'off' !== $raleway ) {
		                $font_families[] = 'Raleway:400,700,500';
		            }

		            if ( 'off' !== $marvel ) {
		                $font_families[] = 'Marvel:400,700';
		            }
		            
		            $query_args = array(
		            'family' => urlencode( implode( '|', $font_families ) ),
		            'subset' => urlencode( 'latin,latin-ext,vietnamese,cyrillic-ext,cyrillic,greek,greek-ext' ),
		        	);
		 
		        	$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
		        }
		    
		    wp_enqueue_style( 'magicbook-default-fonts', esc_url_raw( $fonts_url ), array(), null );

			/**
			 * Enqueue scripts and styles.
			 */

		    $minify_suffix='';
		    if(MAGICBOOK_DEBUG==false){
		 		$minify_suffix='.min';
		    }

			global $MB_VAN;

		    //CSS Files
			wp_enqueue_style("style", get_stylesheet_uri(), false, null, "all");
			if(MAGICBOOK_DEBUG==true){
				wp_enqueue_style("bookblock", get_template_directory_uri()."/css/files/bookblock.css", false, null, "all");
				wp_enqueue_style("component", get_template_directory_uri()."/css/files/component.css", false, null, "all");
				wp_enqueue_style("bjqs", get_template_directory_uri()."/css/files/bjqs.css", false, null, "all");
				wp_enqueue_style("perfect-scrollbar", get_template_directory_uri()."/css/files/perfect-scrollbar.min.css", false, null, "all");
				wp_enqueue_style("reset", get_template_directory_uri()."/css/files/reset.css", false, null, "all");
				wp_enqueue_style("main", get_template_directory_uri()."/css/files/main.css", false, null, "all");
				wp_enqueue_style("custom", get_template_directory_uri()."/custom.css", false, null, "all");
				wp_enqueue_style("layout-responsive", get_template_directory_uri()."/css/files/responsive.css", false, null, "all");
		    }else{
		    	wp_enqueue_style("magicbook", get_template_directory_uri()."/css/magicbook.min.css", false, null, "all");
		    }
			
			wp_enqueue_style("font-awesome", get_template_directory_uri()."/font-awesome/css/font-awesome.min.css", false, null, "all");
			wp_enqueue_style("flexslider", get_template_directory_uri()."/js/vendors/flexslider/flexslider.css", false, null, "all");
			wp_enqueue_style("colorbox", get_template_directory_uri()."/js/vendors/colorbox/colorbox.css", false, "1.2", "all");
			
			//JS Files
			wp_enqueue_script( 'easing', get_template_directory_uri() . '/js/vendors/jquery.easing.min.js', array( 'jquery' ), null, false );
		    wp_enqueue_script( 'modernizr-custom', get_template_directory_uri() . '/js/modernizr.custom.38010.js', array( 'jquery' ), null, false );
		    wp_enqueue_script( 'isotope', get_template_directory_uri() . '/js/vendors/jquery.isotope.min.js', array( 'jquery' ), null, false );
		    wp_enqueue_script( 'hoverIntent');

		    if(MAGICBOOK_DEBUG == true){
				wp_enqueue_script( 'bookblock', get_template_directory_uri() . '/js/vendors/jquery.bookblock.js', array( 'jquery' ), null, true );
				wp_enqueue_script( 'colorbox', get_template_directory_uri() . '/js/vendors/colorbox/jquery.colorbox.js', array( 'jquery' ), null, true );
				wp_enqueue_script( 'bjqs', get_template_directory_uri() . '/js/vendors/bjqs-1.3.js', array( 'jquery' ), null, true );
				wp_enqueue_script( 'jquerypp-custom', get_template_directory_uri() . '/js/vendors/jquerypp.custom.js', array( 'jquery' ), null, true );
				wp_enqueue_script( 'placeholder', get_template_directory_uri() . '/js/vendors/jquery.placeholder.js', array( 'jquery' ), null, true );
				wp_enqueue_script( 'perfect-scrollbar', get_template_directory_uri() . '/js/vendors/perfect-scrollbar.min.js', array( 'jquery' ), null, true );
				wp_enqueue_script( 'mousewheel', get_template_directory_uri() . '/js/vendors/jquery.mousewheel.js', array( 'jquery' ), null, true );
				wp_enqueue_script( 'classie', get_template_directory_uri() . '/js/vendors/classie.js', array( 'jquery' ), null, true );
				wp_enqueue_script( 'imagesLoaded', get_template_directory_uri() . '/js/vendors/jquery.imagesLoaded.min.js', array( 'jquery' ), null, true );
				wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/js/vendors/flexslider/jquery.flexslider-min.js', array( 'jquery' ), null, true );
				wp_enqueue_script( 'magicbook-wp', get_template_directory_uri() . '/js/custom-for-wp.js', array( 'jquery' ), null, true );
			}else{
				wp_enqueue_script( 'magicbook-plugins', get_template_directory_uri() . '/js/plugins.min.js', array( 'jquery' ), null, true );
			}
			if($MB_VAN['default_menu_button']=='0' || !isset($MB_VAN['default_menu_button'])):
			    wp_enqueue_script( 'pushy', get_template_directory_uri() . '/js/pushy.js', array( 'jquery' ), null, true );
			endif;
			
			wp_enqueue_script( 'magicbook-wp', get_template_directory_uri() . '/js/custom-for-wp.js', array( 'jquery' ), null, true );
			if(is_home() && $MB_VAN['book-homepage']=='1'){
				wp_enqueue_script( 'magicbook', get_template_directory_uri() . '/js/script'.$minify_suffix.'.js', array( 'jquery' ), null, true );
				
				$flip_direction = isset($MB_VAN['flip_direction'])?$MB_VAN['flip_direction']:'rtl';
				$initOpen = isset($MB_VAN['initOpen'])?$MB_VAN['initOpen']:'0';
				$menuText = apply_filters('magicbook_menu_text',esc_html__('Menu','magicbook'));
				$showMenuText = isset($MB_VAN['show_menu_text'])? $MB_VAN['show_menu_text']: '0';
				$closeAction = isset($MB_VAN['closeAction'])? $MB_VAN['closeAction']: '0';

				wp_add_inline_script( 'magicbook','template_url="'.get_template_directory_uri().'";
			    is_home=1;initOpen='.$initOpen.';flip_direction="'.$flip_direction.'";menuText="'.$menuText.'";showMenuText="'.$showMenuText.'";closeAction="'.$closeAction.'";','before');
			}
		}

		/**
		 * Enqueue child theme stylesheet.
		 * A separate function is required as the child theme css needs to be enqueued _after_ the parent theme
		 * MagicBook css and the separate WooCommerce css.
		 *
		 * @since  1.0
		 */
		public function child_scripts() {
			if ( is_child_theme() ) {
				wp_enqueue_style( 'magicbook-child-style', get_stylesheet_uri(), '' );
			}
		}

		/**
		 * Load Scripts in WP admin	
		 * @since 1.0
		 */
		public function admin_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_style( 'farbtastic' );
			wp_enqueue_script('farbtastic' );
			wp_enqueue_style("magicbook-admin", MAGICBOOK_INC_URI."css/admin.css", false, "1.0", "all");
			wp_enqueue_script("magicbook-admin", MAGICBOOK_INC_URI."js/admin_script.js");
		}

		/* Load Framework
		 * Include the framework function files.
		 * since 1.0
		 */
		public function load_files(){
	
			/**
			 * Functions.
			 */
			require_once(MAGICBOOK_INC_DIR.'functions/common-functions.php');	
			require_once(MAGICBOOK_INC_DIR."functions/admin-functions.php");
			require_once(MAGICBOOK_INC_DIR."functions/theme-functions.php");
			require_once(MAGICBOOK_THEME_DIR."/vc_extends/vc_extended.php"); 

			
			/**
			 * Include admin screen
			 */
			require_once(MAGICBOOK_INC_DIR.'admin/class-admin.php');
			require_once(MAGICBOOK_INC_DIR.'admin/options-init.php');

			/**
			 * Plugins
			 */
			require_once(MAGICBOOK_INC_DIR.'admin/plugins_list.php');

			/*Portfolio settings*/
			require_once(MAGICBOOK_INC_DIR."extensions/portfolio-settings.php"); 

			/*Add live customize support*/
			require_once(MAGICBOOK_INC_DIR.'customize/customize.php');

			/*Demo Importer*/
			require_once(MAGICBOOK_INC_DIR."admin/demo-importer.php"); 

			/* WordPress Core Plugin*/
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); 

		}

		/**
		 * Check Theme Updates
		 */
		public function check_update(){
			if( MagicBookAdmin::check_license()) {
		      require_once(MAGICBOOK_INC_DIR.'admin/theme-update-checker.php');
			  $theme_update_checker = new ThemeUpdateChecker(
			    'magicbook',
			    'https://www.themevan.com/update-server/?action=get_metadata&slug=magicbook'
			  );
		    }
		}
		

		/**
		 * Returns the instance.
		 *
		 * @since  1.0.0
		 * @return object
		 */
		public static function get_instance() {

			// If the single instance hasn't been set, set it now.
			if ( null == self::$instance ) {
				self::$instance = new self;
			}

			return self::$instance;
		}

	}
}

function MagicBook() {
   	return MagicBook::get_instance();
}

function magicbook_user_agent() {
    return 'magicbook-user-agent';
}

$MB_VAN = get_option( 'magicbook_opt' );

return MagicBook();