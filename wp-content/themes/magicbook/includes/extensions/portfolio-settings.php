<?php
/**
 * Portfolio Post Type
 * @package VAN Framework
 */
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'magicbook_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @package  MagicBook
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */


/**
 * Conditionally displays a metabox when used as a callback in the 'show_on_cb' cmb2_box parameter
 *
 * @param  CMB2 object $cmb CMB2 object
 *
 * @return bool             True if metabox should show
 */
if ( ! function_exists( 'magicbook_show_if_front_page' ) ) :
function magicbook_show_if_front_page( $cmb ) {
	// Don't show this metabox if it's not the front page template
	if ( $cmb->object_id !== get_option( 'page_on_front' ) ) {
		return false;
	}
	return true;
}
endif;

/**
 * Conditionally displays a field when used as a callback in the 'show_on_cb' field parameter
 *
 * @param  CMB2_Field object $field Field object
 *
 * @return bool                     True if metabox should show
 */
if ( ! function_exists( 'magicbook_hide_if_no_cats' ) ) :
function magicbook_hide_if_no_cats( $field ) {
	// Don't show this field if not in the cats category
	if ( ! has_tag( 'cats', $field->object_id ) ) {
		return false;
	}
	return true;
}
endif;

/**
 * Conditionally displays a message if the $post_id is 2
 *
 * @param  array             $field_args Array of field parameters
 * @param  CMB2_Field object $field      Field object
 */
if ( ! function_exists( 'magicbook_before_row_if_2' ) ) :
function magicbook_before_row_if_2( $field_args, $field ) {
	if ( 2 == $field->object_id ) {
		echo '<p>Testing <b>"before_row"</b> parameter (on $post_id 2)</p>';
	} else {
		echo '<p>Testing <b>"before_row"</b> parameter (<b>NOT</b> on $post_id 2)</p>';
	}
}
endif;

/**
 * Get the Wordpress Menu
 */
if ( ! function_exists( 'magicbook_get_menus' ) ) :
function magicbook_get_menus(){
    $menus = get_terms('nav_menu');
    $menu_options = array('Default Menu');
	foreach($menus as $menu){
	  $menu_options[$menu->slug] = $menu->name;
	} 
	return $menu_options;
}
endif;

/**
 * Hook in and add a details metabox.
 * Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
 */
add_action( 'cmb2_admin_init', 'magicbook_register_details_metabox' );
if ( ! function_exists( 'magicbook_register_details_metabox' ) ) :
function magicbook_register_details_metabox() {

  // Start with an underscore to hide fields from custom fields list
  $suffix = '_value';


  /**
   * Portfolio settings
   */
  $cmb_magicbook_page = new_cmb2_box( array(
	'id'            => 'portfolio_metabox'.$suffix,
	'title'         => esc_html__( 'Portfolio Settings', 'magicbook' ),
	'object_types'  => array( 'portfolio'), // Post type
	'priority'   => 'default',
  ) );
  
  $cmb_magicbook_page->add_field( array(
	'name'             => esc_html__( 'Portfolio Type', 'magicbook' ),
	'id'               => 'portfolio_type'.$suffix,
	'type'             => 'select',
	'default'		   => 'image',
	'options'		   => array(
		'image' => esc_html__('Image', 'magicbook'),
		'video' => esc_html__('Video', 'magicbook'),
		'audio' => esc_html__('Audio', 'magicbook'),
	),
  ) );

  $cmb_magicbook_page->add_field( array(
	'name'             => esc_html__( 'Portfolio Slider', 'magicbook' ),
	'desc'             => esc_html__( 'After you disabled the slideshow, all images will be laid end to end.', 'magicbook' ),
	'id'               => 'portfolio_slider'.$suffix,
	'type'             => 'select',
	'default'		   => 'No',
	'options'		   => array(
		'Yes' => esc_html__('Yes','magicbook'),
		'No' => esc_html__('No','magicbook'),
	),

  ) );

  $cmb_magicbook_page->add_field( array(
	'name'             => esc_html__( 'Portfolio FullWidth', 'magicbook' ),
	'desc'             => esc_html__( 'Display full width Slider.', 'magicbook' ),
	'id'               => 'portfolio_fullwidth'.$suffix,
	'type'             => 'select',
	'default'		   => 'No',
	'options'		   => array(
		'Yes' => esc_html__('Yes','magicbook'),
		'No' => esc_html__('No','magicbook'),
	),
  ) );

  $cmb_magicbook_page->add_field( array(
	'name'             => esc_html__( 'Portfolio Video (Youtube/Vimeo)', 'magicbook' ),
	'desc'             => '<a href="'.get_template_directory_uri().'/includes/extensions/help/video.jpg'.'" target="_blank">'.esc_html__('How to get Youtube/Vimeo code?','magicbook').'</a>',
	'id'               => 'portfolio_video'.$suffix,
	'type'             => 'textarea_code'
  ) );

  $cmb_magicbook_page->add_field( array(
	'name'             => esc_html__( 'Portfolio Audio (SoundCloud)', 'magicbook' ),
	'desc'             => '<a href="'.get_template_directory_uri().'/includes/extensions/help/sc.jpg'.'" target="_blank">'.esc_html__('Example: How to get SoundCloud code?','magicbook').'</a> - <a href="http://soundcloud.com" target="_blank">'.esc_html__('Go to SoundCloud now!','magicbook').'</a>',
	'id'               => 'portfolio_audio'.$suffix,
	'type'             => 'textarea_code'
  ) );

}
endif;