<?php
vc_map( array(
   "name" => __("MagicBook Container",'magicbook-addon'),
   "base" => "van_magicbook_container",
   "class" => "wpb_magicbook_container",
   "icon" =>"icon-wpb-magicbook_container",
   "category" => __('MagicBook','magicbook-addon'),
   'admin_enqueue_js' => MBA_DIR_URI.'assets/js/vc.js',
   'admin_enqueue_css' => MBA_DIR_URI.'assets/css/magicbook-vc.css',
   'custom_markup'=>'',
   "js_view" => 'VcColumnView',
   "show_settings_on_create" => true,
   "content_element" => true,
   "is_container" => true,
   "as_parent" => array('except' => 'van_magicbook_container'),
   "params" => array(
      array(
         "type" => "colorpicker",
         "class" => "wpb_van_magicbook_container_bg_color",
         "heading" => __("Background Color",'magicbook-addon'),
         "param_name" => "bg_color",
         "value" => '',
         "description" => __("Set the background color for this container.",'magicbook-addon')
      ),
      array(
         "type" => "attach_image",
         "class" => "wpb_van_magicbook_container_bg_picture",
         "heading" => __("Background Picture",'magicbook-addon'),
         "param_name" => "bg_picture",
         "value" => '',
         "description" => __("Set the background picture for this container.",'magicbook-addon')
      ),
      array(
         "type" => "dropdown",
         "class" => "wpb_van_magicbook_container_bg_size",
         "heading" => __("Background Size",'magicbook-addon'),
         "param_name" => "bg_size",
         "value" => array('auto','cover','100%')
      ),
      array(
         "type" => "colorpicker",
         "class" => "wpb_van_magicbook_container_text_color",
         "heading" => __("Text Color",'magicbook-addon'),
         "param_name" => "text_color",
         "value" => '',
         "description" => __("Set the text color for this container.",'magicbook-addon')
      ),
      array(
         "type" => "dropdown",
         "class" => "wpb_van_magicbook_container_height",
         "heading" => __("Set the height to 100%",'magicbook-addon'),
         "param_name" => "fullsize",
         "value" => array(
             __('Yes','magicbook-addon') => '1',
             __('No','magicbook-addon') => '0',
         ),
         "description" => __("Set the size of this container as same as the book page.",'magicbook-addon')
      ),
   )
) );
// A must for container functionality, replace Wbc_Item with your base name from mapping for parent container
if(class_exists('WPBakeryShortCodesContainer')){
   class WPBakeryShortCode_van_magicbook_container extends WPBakeryShortCodesContainer {}
}

/*Custom testimonial*/
function van_magicbook_container_shortcode( $atts, $content) {
   extract(shortcode_atts(array(
		'bg_picture' => '',
      'bg_color' => '',
      'text_color'=>'',
      'fullsize'=>'1',
      'bg_size' => 'auto',
	), $atts));
   $data = '';
   
   if($bg_color<>''){
      $data = 'data-bg-color="'.$bg_color.'"';
   }
   if($text_color<>''){
      $data .= ' data-text-color="'.$text_color.'"';
   }
   if($bg_picture<>''){
      $data.= ' data-bg-img="'.wp_get_attachment_url($bg_picture).'"';
   }
   if($bg_size<>''){
      $data.=' data-bg-size="'.$bg_size.'"';
   }
   if($fullsize<>''){
      $data.=' data-fullsize="'.$fullsize.'"';
   }
   $str=' <div class="magicbook-wrapper" '.$data.'><div class="magicbook-inner-wrapper">'.$content.'</div></div>'.PHP_EOL;
   return $str;
}
add_shortcode( 'van_magicbook_container', 'van_magicbook_container_shortcode' );