<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress_dump' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '##avMP:m{dkN3<4KQjB}ruD&F7Q0Ws/c)+A;q/L>gp~Zh+IWc!!%_6bl>&SLESQW' );
define( 'SECURE_AUTH_KEY',  'G>8k@1/:n,U`5?{_$2JJtUEmu8jx+1isT:XxFbmcI_f:S!<!xCJ^H>gdKWa!5DX(' );
define( 'LOGGED_IN_KEY',    ';W/gC>VaKr4N,3(ux^avHhQ2_th!LqWq(~=S.|1Le& m&-^$IK/vHDA:9(TEKl%t' );
define( 'NONCE_KEY',        'S.-h<@1V5t7Iq^s.qlx1P.-i-vuGsXHcvu@@8<ETH3kb},3e;ZQRUDKqe:(k}VG6' );
define( 'AUTH_SALT',        '@&yWl#1$&pKIA7$,tQ~RnlGUQ1PicSIH=t]N4Cjjp~gVJ*i3_}e*AQW tJ;9dTTC' );
define( 'SECURE_AUTH_SALT', 'x,nQPAW W;/-0PrfyiQb1dnj<Sj>P!v^ISfV]} G?<{i8I<sc..W;q(H@;=l2V.B' );
define( 'LOGGED_IN_SALT',   '9)v1Ju5IAbsZ+/J%R4}+$C)s%e@z&HhNy2cIUJ3dB|0YRE$D<YQ}2V2`yQ6SymPp' );
define( 'NONCE_SALT',       '3zj&Tk?jMFQoA]45CRnH~qrJ4BcHqjpswqY4+bw%vPZQ[WBsCX!?4eUKQlhm0`T7' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
